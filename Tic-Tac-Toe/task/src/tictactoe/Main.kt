package tictactoe

import java.util.Scanner;
import kotlin.math.abs
import kotlin.streams.toList

val winningCombinations = setOf<Array<Coordinates>>(
    arrayOf(Coordinates(0, 0), Coordinates(0, 1), Coordinates(0, 2)),
    arrayOf(Coordinates(0, 0), Coordinates(1, 1), Coordinates(2, 2)),
    arrayOf(Coordinates(0, 0), Coordinates(1, 0), Coordinates(2, 0)),
    arrayOf(Coordinates(0, 1), Coordinates(1, 1), Coordinates(2, 1)),
    arrayOf(Coordinates(0, 2), Coordinates(1, 1), Coordinates(2, 0)),
    arrayOf(Coordinates(0, 2), Coordinates(1, 2), Coordinates(2, 2)),
    arrayOf(Coordinates(1, 0), Coordinates(1, 1), Coordinates(1, 2)),
    arrayOf(Coordinates(2, 0), Coordinates(2, 1), Coordinates(2, 2))
)

fun main() {
    val gameField = Array(3) { CharArray(3) { c -> ' ' } }
    printGame(gameField)

    var user = 'X'
    do {
        val coordinates = getUserInput(gameField)
        gameField[coordinates.x][coordinates.y] = user
        user = if (user == 'X') 'O' else 'X'
        printGame(gameField)
    } while (validate(gameField))
}

fun validateInputFormat(input: List<String>, field: Array<CharArray>): Pair<Boolean, String> {
    try {
        val testedInput = input.stream()
            .map { it.toInt() - 1 }
            .filter { it in 0..2 }.toList()

        if (testedInput.size == 2) {
            if (field[testedInput[0]][testedInput[1]] != ' ') {
                return Pair(false, "This cell is occupied! Choose another one!")
            }
        } else {
            return Pair(false, "Coordinates should be from 1 to 3!")
        }
    } catch (e: Exception) {
        return Pair(false, "You should enter numbers!")
    }
    return Pair(true, "")
}

private fun printGame(input: Array<CharArray>) {
    println("---------")
    input.forEach { chars ->
        print("| ")
        chars.forEach { char -> print("$char ") }
        println("|")
    }
    println("---------")
}

fun validate(input: Array<CharArray>): Boolean {
    val threeXInARow = checkAnyOneWon(input, 'X')
    val threeOInARow = checkAnyOneWon(input, 'O')

    val countX = countCharacter(input, 'X')
    val countO = countCharacter(input, 'O')

    if (threeXInARow && threeOInARow || abs(countX - countO) >= 2) {
        println("Impossible")
    } else if (threeXInARow) {
        println("X wins")
    } else if (threeOInARow) {
        println("O wins")
    } else if (input.flatMap { it.toList() }.contains(' ')) {
        return true
    } else {
        println("Draw")
    }
    return false
}

fun checkAnyOneWon(input: Array<CharArray>, player: Char): Boolean {
    return winningCombinations.any {
        it.all { coor -> input[coor.x][coor.y] == player }
    }
}

fun countCharacter(input: Array<CharArray>, player: Char): Int {
    return input.flatMap { it.toList() }.filter { char -> char == player }.count()
}

fun getUserInput(input: Array<CharArray>): Coordinates {
    print("Enter the coordinates: ")
    val scanner = Scanner(System.`in`)
    val values = scanner.nextLine().split(" ")
    val (formatCorrect, errorMessage) = validateInputFormat(values, input)

    if (!formatCorrect) {
        println(errorMessage)
        return getUserInput(input)
    }

    return Coordinates(values[0].toInt() - 1, values[1].toInt() - 1)
}

data class Coordinates(
    val x: Int,
    val y: Int
) {
    init {
        require(x in 0..2)
        require(y in 0..2)
    }
}
